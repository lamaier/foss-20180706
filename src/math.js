/**
 * Divide numbers
 * @return {Number}   The result
 */
function div (a, b) {
  
  return a/b
}

/**
 * Substract numbers
 * @return {Number}   The result
 */
function sub (a, b) {
  
  return a-b
}

/**
 * Add numbers
 * @return {Number}   The result
 */
function add (a, b) {
  
  return a+b
}

/**
 * Multiply numbers
 * @return {Number}   The result
 */
function mult (a, b) {
  
  return a*b
}

/**
 * The conway sequence looks like: 1 11 21 1211 111221 ...
 * @param  {String} s Initial value
 * @return {String}   Next value or null if the initial value is invalid
 */
function conwayNext (s) {
  // TODO
  return ''
}

module.exports = {
  mult,
  add,
  div,
  sub,
  conwayNext
}
